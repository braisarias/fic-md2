Variable n: nat.
Check n.
Check 0.
Check O.
Check S(0).
Check S.
Check gt.
Definition uno := (S O).
Check uno.
Print uno.
Definition dos : nat := S (S O).
Check dos.
Print dos.
Definition tres:= S( S( S O)):nat.
Check tres.
Print tres.
Definition caca := S O.
Print caca.
Check plus.
Definition double1 := fun m:nat => plus m m.
Check double1.
Print double1.
Definition double2 (m:nat) := plus m m.
Print double2.
Check double2 0.
Eval compute in double2 (O).
Eval compute in double2 (S O).
Eval compute in double2 (tres).
Check (forall m:nat, gt m 0).

