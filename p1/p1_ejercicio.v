(* Arias Rio, Brais brais.arias *)

(* Pérez Sueiro, Mónica monica.psueiro *)



(* 
TRABAJANDO CON LOGICA PROPOSICIONAL EN COQ

Podemos utilizar COQ para demostrar teoremas en lógica proposicional. 

Utilizaremos los operadores ~ (NO), \/ (O), /\ (Y), -> (Implicacion) y
<-> (doble implicacion).
*)

(*En logica proposicional, una variable es cierta o falsa. Esto no es cierto
en todas las lógicas, así que se le decimos a COQ que lo asuma*)

Axiom ExcludedMiddle : forall P : Prop, P \/ ~ P. 

Section Propositional_Logic_Intro.

(*************
NEGACIÓN:
*************
Coq maneja la negación como una implicación (->) a una proposición especial,
False. 

Es decir: ~P se define como P -> False.

Utilizamos la táctica "unfold" para reescribir la negación como implicación
y poder trabajar con ella. 

False nunca puede ser cierto. Si lo es, entonces hemos llegado a una contradicción
*)

Lemma NoFalse : ~ False.
Proof.
  (* Unfold reemplaza un identificador por su definicion*)
  unfold not.
  (* "Intro" es útil para trabajar con implicaciones. 
     En una implicación, si el antencedente no se cumple, entonces
     la proposición es trivialmente cierta (da igual el valor de la consecuencia.
     Intro introduce el antecedente de la implicación y asume que es cierto.
     A partir de ahí, tenemos que demostrar el consecuente. 
  *)
  intro FALSE.
  (*Para el caso en el que False es falso, entonces la proposición es cierta.
    False no puede ser cierto, así que terminamos la demostración por reducción
    al absurdo (existe una contradicción)*)
  contradiction. 
Qed.

(*
Negar dos veces una variable es igual a no negarla ninguna vez.
Es decir, es lo mismo escribir P que escribir ~~P. 
*)
Theorem Double_Negation: forall(P : Prop), (P) -> (~~P).
Proof.
  (*Intro reemplaza el forall (P : Prop) por una proposición cualquiera, P.*)
  intro P. 
  unfold not.
  (*Si P es falso la fórmula es cierta seguro. Para seguir la prueba, asumimos P es cierto*)
  intro P_TRUE.
  intro IF_P_TRUE_THEN_FALSE.
  (*Apply cambia nuestra meta actual utilizando una implicación: 
    Si queremos demostrar B. 
    Y sabemos que A -> B.
    Entonces para demostrar B nos llega demostrar A*)
  apply IF_P_TRUE_THEN_FALSE.
  (*Ya sabemos que P es cierto*) 
  apply P_TRUE.
Qed.

Theorem Contradiction_All_True: forall(A B: Prop), (A /\ ~A) -> B.
Proof.
  (*Tenemos que demostrar el teorema para cualquier variable A y B.*)
  intros A B. 
  intro A_TRUE_AND_A_FALSE.
  (*Tenemos una hipótesis compleja, está formada por una conjunción.
    Para trabajar con ella, podemos partirla en dos utilizando la táctica
    destruct.
  *) 
  destruct A_TRUE_AND_A_FALSE as [A_TRUE A_FALSE].
  (*Las táctica unfold también se puede usar en hipótesis*)
  unfold not in A_FALSE.
  (* La táctica "apply...in" nos permite razonar utilizando dos hipótesis.
     Tenemos: A_FALSE: A -> False. 
     y A_TRUE: A.
     Dado que A es cierto, podemos utilizar la implicación de A_FALSE 
     para concluir FALSE.
   *)
  apply A_FALSE in A_TRUE.
  (*Tenemos False como hipótesis. False no puede ser cierto, así 
    que podemos terminar la prueba por reducción al absurdo utilizando
    "contradiction"*)
  contradiction.
Qed.

(*************
Ejercicios de negaciones e implicaciones
*************

Utilizando las tácticas que acabamos de ver, demuestra que la implicación
es transitiva y contrapositiva (si P -> Q, entonces ~Q -> ~P).*)

(*Transitividad*)
Theorem Transitive: forall (A B C : Prop), ((A -> B) /\ (B -> C)) -> (A -> C).
Proof.

(* subimos todo o que podemos *)
intros A B C.
intro ANDS.
intro A_TRUE.
(* dividimos ANDS para poder tratala *)
destruct ANDS as [A_IMPLIES_B B_IMPLIES_C].
(* como temos A true, aplicando A->B temos B *)
apply A_IMPLIES_B in A_TRUE.
(* como temos B true, aplicando B->C temos C *)
apply B_IMPLIES_C in A_TRUE.
apply A_TRUE.
Qed.

Theorem Contrapositive: forall(P Q : Prop), (P -> Q) -> (~Q -> ~P).
Proof.
(* subimos *)
intros P Q.
(* quitamos negaciones *)
unfold not.
(* subimos todo o que se pode *)
intros.
(* con isto obtemos Q, para aplicar na outra implicacion *)
apply H in H1.
(* aplicamos Q para ter false *)
apply H0 in H1.
(* xa temos o goal nunha hipotese *)
apply H1.
Qed.

(*Demuestra que una proposicion no puede ser cierta y falsa
  al mismo tiempo*)
Theorem NotTrueAndFalse: forall(P : Prop), ~ (P /\ ~ P). 
Proof.
(* quitamos as negacions *)
unfold not.
(* subimos todo *)
intros.
(* dividimos H *)
destruct H.
(* temos P, entón aplicamola na implicacion para ter false *)
apply H0 in H.
apply H.
Qed.

(* Demuestra la regla básica de deducción en lógica propocional:
Si A -> B
y A es cierto
entonces B es cierto
*)
Theorem ModusPonens : forall (A B : Prop), A /\ (A -> B) -> B. 
Proof.

(* subimos todo *)
intros.
(* dividimos H *)
destruct H.
(* como temos A, aplicamos na implicacion para ter B *)
apply H0 in H.
apply H.

Qed.
(* Demuestra la regla inversa. 
Si A -> B
y B es falso
entonces A tiene que ser falso *)
Theorem ModusTollens: forall (A B: Prop), ~B /\ (A -> B) -> ~A.
Proof.
(* subimos A B *)
intros A B.
(* cambia a negación pola implicacion a false *)
unfold not. 
(* subimos o ANd  *)
intro AND.

(* prova por reducion ao absurdo *)
intro A_TRUE.
(* dividimos o AND *)
destruct AND as [B_IMPLIES_FALSE A_IMPLIES_B].
(* como temos o A, aplicamos na A->B, para ter B *)
apply A_IMPLIES_B in A_TRUE.
(* ao ter B, aplicamos na implicacion para ter falso *)
apply B_IMPLIES_FALSE in A_TRUE.
apply A_TRUE.


Qed.

(* Demostrar que dos proposiciones son equivalentes

La implicación se puede reprentar utilizando los operadores ~ y /\

A -> B es lógicamente equivalente a ~A \/ B). Es decir, una implicación
es cierta si no se cumple el antecedente A (el valor de B da igual) o si
se cumple el antecedente A y el consecuente B. 

Para demostrar que dos proposiciones son equivalentes, tenemos que demostrar
que una implica a la otra y viceversa: A <-> B. 
 *)
Theorem Implication_Negation : forall (A B : Prop), (A -> B) -> (~A \/ B).
Proof.

intro A.
intro B. 
intro A_IMPLIES_B.
unfold not.
(*Como trabajar con disyunciones.
Utilizamos la directiva destruct(ExcludedMiddle A) para
decirle a COQ que queremos seguir la prueba por dos casos: en uno asumimos
que A es cierto y en otra asumimos que A es falso.
 *)
destruct(ExcludedMiddle A).
(*En el caso de que A sea cierto, seguro que la parte izquierda del OR
no se cumple. Seguimos por la derecha (nos llega con probar una, es una disyunción*)
right.
apply A_IMPLIES_B in H.
apply H.


(* En el caso de que A sea falso se cumple la parte izquierda del OR directamente *)
left.
unfold not in H.
apply H.
Qed.

(* Demuestra la doble implicacion en el sentido contrario *)
Theorem Negation_implication: forall (A B : Prop), (~A \/ B) -> (A -> B).
Proof.

unfold not.
intros A B.
intros.

destruct H.
(* temos de hipotese A, e tamén a-> false, entón temos contradiccion *)
contradiction.
apply H.

Qed.
(*Juntamos los dos resultados*)

Theorem Implication: forall (A B : Prop), (A -> B) <-> (~A \/ B).
Proof.
intro A.
intro B.
(*Split tambien se usa para la doble implicacion*)
split.
(*Podemos utilizar los teoremas anteriores*)
apply Implication_Negation.
apply Negation_implication.
Qed.


(*Lema auxiliar: idempotente do OR *)
Lemma idempotent_or : forall (P:Prop), (P \/ P) -> P.
Proof.
(* subimos todo o que podemos *)
intros.
(* dividimos en dous H *)
destruct H.
(* aqui xa temos os goal nas hipotesese; tamén poderiamos facer assumption *)
apply H.
apply H.
Qed.


(* Vamos a utilizar lo que hemos aprendido para resolver un ejercicio
de logica. Completa lo que falta para terminar las demostraciones.
 *)
Section Propositional_Logic_Exercise.
(* Formalizando un problema:
Un viajero llega a una ciudad desconocida.
En esta ciudad solo viven caballeros y ladrones. 
Los ladrones siempre mienten.
Los caballeros siempre dicen la verdad.
Llega a la plaza del pueblo y se encuentra con dos personas:
A y B. 
Se produce el siguiente dialogo: 
Viajero: A, ¿eres ladron o caballero?
A: (murmullo).
Viajero: B, ¿que ha dicho?
B: Ha dicho que es un ladron.

¿B es ladron o caballero?


Nadie puede decir de si mismo que es un ladron. Los ladrones
siempre mienten, asi que siempre diran que son caballeros.
Asi que B miente, es un ladron. Vamos a demostrarlo

Basado en: http://psnively.github.com/blog/2012/08/27/Propositional_Logic_in_Coq/
*) 
Section Ladron_o_Caballero.
  (*Definimos el tipo habitante*)
  Variable Habitante : Set.
  (*Tenemos dos habitantes, A y B. Queremos demostrar que B es un ladron*)
  Variables A B : Habitante.
  (*Ladron es una función que dice, dado un habitante si es ladron (True) o no (False)*)
  Variable Ladron : Habitante -> Prop.
  (* Un habitanet es un ladron si y sólo si lo que dice es falso*)
  Definition dice (n : Habitante) (P : Prop) := Ladron n <-> ~P.
  (*B dice que A es un ladrón*)
  Hypothesis B_dice: dice B (dice A (Ladron A)).

  (*Lema auxiliar para demostrar que B es un ladrón completa la demostración*)
Lemma Contradiction : forall (P: Prop), ~(P <-> ~P).
Proof.

(* subimos todo o que podemos *)
intros.
intro.
(* separar H para poder tratala *)
destruct H.
(* aplicamos Implication_Negation, para quitar a implicacion *)
apply Implication_Negation in H.
(* como temos o idempotente do OR, aplicamos o lema que o desfai *)
apply idempotent_or in H.
(* quitamos o not en H *)
unfold not in H.
(* aplicamos H para ter P de goal *)
apply H.
apply H0.

assumption.



Admitted.

Theorem B_Ladron: Ladron B.
unfold dice in B_dice.
destruct B_dice.
apply H0.
apply Contradiction.
Qed.

End Ladron_o_Caballero.


End Propositional_Logic_Exercise.
