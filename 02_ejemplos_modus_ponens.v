(* EJEMPLOS CON MODUS PONENS *)

Section EjemplosModusPonens.


(*** Ejemplo VIIa ***********************)

Section EjemploVIIa.

Theorem modusponens_a: forall (A B:Prop), A->(A->B)->B. 
Proof.
intros. (*subimos todo o que podemos arriba *)
apply H0.	(* 1a opción, aplicar Apply *)
(* agora pasamos a ter que demostrar A*)
assumption.
Qed.

End EjemploVIIa.

Check modusponens_a.
Print modusponens_a.


(*** Ejemplo VIIb ***********************)

Section EjemploVIIb.

Theorem modusponens_b: forall (A B:Prop), A->(A->B)->B. 
Proof.
intros.
cut A.	(* 2a opción, aplicar Cut *)
(*Cut pasaba de demostrar B, a demostrar que B->A, a A->B e A*)
assumption.
assumption.
Qed.

End EjemploVIIb.

Check modusponens_b.
Print modusponens_b.


(*** Ejemplo VIIc ***********************)

Section EjemploVIIc.

Theorem modusponens_c: forall (A B:Prop), A->(A->B)->B. 
Proof.
intros.
exact (H0 H).	(* 3a opción, aplicar Exact *)
(* indicamoslle un termo do contexto *)
Qed.

End EjemploVIIc.

Check modusponens_c.
Print modusponens_c.


(*** Ejemplo VIId ***********************)

Section EjemploVIId.

Theorem modusponens_d: forall (A B:Prop), A->(A->B)->B. 
Proof.
intros.
auto.	(* 4a opción, aplicar Auto *)
Qed.

End EjemploVIId.

Check modusponens_d.
Print modusponens_d.



Section EjemplosModusPonens.

