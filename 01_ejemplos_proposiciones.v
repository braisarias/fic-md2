(* EJEMPLOS DE CÁLCULOS DE PROPOSICIONES *)

(*** Ejemplo I ***************************)

Section EjemploI. (* empleare secciones para evitar conflictos *)

Hypothesis A:Prop. (* declaramos hipotese, co cual esta é asumida como certa *)

(* Queremos demostrar que A implica A  *)
(* Por orde de importacia: *)
(* Theorem nome:__o_que_queremos_demostrar___ . *)
(* cando apliquemos tacticas vainos criar un obxecto q vai ter ese mesmo nome *)

(* Lemma nome:__o_que_queremos_demostrar___*)

(* Goal __o_que_queremos_demostrar___ *)
(* isto é para usar e tirar: unamed theorem *)
(* se logo queremos darlle nome, podemos usar: *)
(* Save nome*)


Lemma var: A->A.
(* o que temos arriba da barra horizontal é o contexto *)
(* o que esta por baixo é o q queremos demostrar  *)
(*  o que temos á dereita é o no subgoal no que estamos *)

Proof.
(*intro.*)
(* regra de tipado Lam ; a de teoría correspondese coa regra T-abs *)
(* intro id. darialle ese nome (id)  *)
intro HX.
(* tamen temos intros: fai varios, está máis abaixo*)
assumption. (* tactica assumption: *)
Qed.
Check var.
Print var.

End EjemploI.


(*** Ejemplo II **************************)

Section EjemploII.

Lemma trivial: forall p:Prop, p->p.
Proof.
(*intros.*)
(*Undo.  no funciona en coqide, usar la flecha arriba en ese caso *)
intros q H.(* subimos todo o que podemos *)
(* podriamos utilizar assumption, pero vamos facer outra*)
exact H.
(* é outra tactica;  baseada en var (como assumption), pero é máis xeral*)
(* exact termo.   *)
Qed.
Check trivial.
(*  *)
Print trivial.

End EjemploII.


(*** Ejemplo IIIa *************************)

Section EjemploIIIa.

Hypothesis A B C: Prop.
Goal (A->(B->C)) -> (A->B) -> (A->C).
(* o coq reescribe isto, recordar que é asociativo pola dereita *)
(* (A -> B -> C) -> (A -> B) -> A -> C *)
(* H: (A->(B->C)); HB: (A->B) H1: A; *)
intros H H0 H1.
(* isto pode obterse co contexto, entón pasamoslla ao exact *)
exact ((H H1) (H0 H1)).
Qed.
(* neste caso, non lle demos nome enton ten un destos por defecto *)
Print Unnamed_thm.

End EjemploIIIa.

(* --------------- *)


Section EjemploIIIb.

Hypothesis A B C: Prop.
Goal (A->(B->C)) -> (A->B) -> (A->C).
(* auto é outra tactica, é automática, primeiros intenta un assumption, *)
(*para ver se hai algo no contexto; se non o hai fai un intros, *)
(*e logo empeza a provar cunha serie de tacticas, a ver se da atopado; *)
(*se non da atopado devolve o control *)
auto.
Save pruebaIIIb.
Print pruebaIIIb.

End EjemploIIIb.

(* --------------- *)

Section EjemploIIIc.

Hypothesis A B C: Prop.
Goal (A->(B->C)) -> (A->B) -> (A->C).
intros H H0 H1.
(* a tactica apply sae da rega app de aplicacion *)
apply H.
Show 2.
(* cuando hay mas de 1 subgoal solo se muestra completamente el primero; *)
(* Show me permite ver completa la i-esima subgoal *)
assumption.
apply H0.
assumption.
Save pruebaIIIc.
Print pruebaIIIc.

End EjemploIIIc.
(* se agora facemos un print de pruebaIIIc, pois saenos algo diferente*)
Print pruebaIIIc.
